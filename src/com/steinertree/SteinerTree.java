package com.steinertree;

import java.util.*;

public class SteinerTree {
    private Graph graph;
    private int[] terminals;

    public SteinerTree(Graph graph, int[] terminals) {
        this.graph = graph;
        this.terminals = terminals;
    }

    public ArrayList<Edge> apply() {
        Map<Integer, Map<Integer, Integer>> graphNodeAdjacency = graph.getAdjacentNodes();
//        System.out.println("ADJ OF 543");
//        System.out.println(graphNodeAdjacency.get(543));
        ArrayList<Integer> addedTerminals = new ArrayList<>();
        addedTerminals.add(this.terminals[0]);
        Map<Integer, Integer> nearestTerminals = new HashMap<>();
        ArrayList<ArrayList<Integer>> finalPaths = new ArrayList<>();
        ArrayList<Map<String, Object>> allPaths = new ArrayList<>();
        Map<String, Object> chosenPath = new HashMap<>();
        while (addedTerminals.size() != this.terminals.length) {
            Map<String, Object> path;
            nearestTerminals.clear();
            allPaths.clear();
            int closestTerminalDist = Integer.MAX_VALUE;
            int closestTerminalNumber = Integer.MAX_VALUE;
            int srcVertex = closestTerminalNumber;
            for (Integer includedTerminalNumber : addedTerminals) {
                path = Dijkstra.apply(graphNodeAdjacency, includedTerminalNumber);
//                System.out.println(path);
                int nearestTerminal = this.getNearestTerminal(path, addedTerminals, includedTerminalNumber);
                nearestTerminals.put(includedTerminalNumber, nearestTerminal);
                int[] distances = (int[]) path.get("distances");
                if (distances[nearestTerminal - 1] < closestTerminalDist) {
                    closestTerminalNumber = nearestTerminal;
                    srcVertex = includedTerminalNumber;
                    closestTerminalDist = distances[nearestTerminal - 1];
                    chosenPath = path;
                }
            }

//            for (i = 0; i < allPaths.size(); ++i) {
//                Map<String, Object> currPath = allPaths.get(i);
//                int[] distances = (int[]) currPath.get("distances");
//                int closestTerminalDist = Integer.MAX_VALUE;
//                int closestTerminalNumber = Integer.MAX_VALUE;
//                int srcVertex = closestTerminalNumber;
//                Iterator it = nearestTerminals.entrySet().iterator();
//                while (it.hasNext()) {
//                    Map.Entry<Integer, Integer> pair = (Map.Entry<Integer, Integer>) it.next();
//                    if (distances[pair.getValue() - 1] < closestTerminalDist) {
//                        closestTerminalNumber = pair.getValue();
//                        srcVertex = pair.getKey();
//                        it.remove();
//                    }
//                }

            addedTerminals.add(closestTerminalNumber);
            Map<Integer, Integer> parents = (Map<Integer, Integer>) chosenPath.get("parents");

            ArrayList<Integer> p = Dijkstra.getPath(closestTerminalNumber, parents, srcVertex);
            System.out.println(p);
            finalPaths.add(p);
        }

        return this.getFinalEdges(graph, finalPaths);

    }


    private ArrayList<Edge> getFinalEdges(Graph graph, ArrayList<ArrayList<Integer>> paths) {
        int pathIndex, nodeIndex;
        ArrayList<Edge> finalEdges = new ArrayList<>();
        for (pathIndex = 0; pathIndex < paths.size(); ++pathIndex) {
//            System.out.println("pathInd: " + paths.get(pathIndex));
            for (nodeIndex = 0; nodeIndex < paths.get(pathIndex).size() - 1; ++nodeIndex) {
                if (!edgeExists(finalEdges, paths.get(pathIndex).get(nodeIndex), paths.get(pathIndex).get(nodeIndex + 1))) {
//                    System.out.println("VERTICES: " + paths.get(pathIndex).get(nodeIndex) + " " + paths.get(pathIndex).get(nodeIndex + 1));
                    Edge edge = graph.getEdge(paths.get(pathIndex).get(nodeIndex), paths.get(pathIndex).get(nodeIndex + 1));
                    finalEdges.add(edge);
//                    if (edge != null) {
//                        finalEdges.add(edge);
//                    }
                }
            }
        }
        return finalEdges;
    }

    private boolean edgeExists(ArrayList<Edge> edges, int srcNode, int destNode) {
//        System.out.println("existence");
//        System.out.println(edges);
//        System.out.println("SRC: " + srcNode);
//        System.out.println("DEST: " + destNode);
        for (Edge edge : edges) {
            edge.showEdge();
            if (edge.getVertices().contains(srcNode) && edge.getVertices().contains(destNode)) {
                return true;
            }
        }
        return false;
    }

    private int getNearestTerminal(Map<String, Object> path, ArrayList<Integer> addedTerminals, int srcVertex) {
        int[] distances = (int[]) path.get("distances");
        int minDistVertex = Integer.MAX_VALUE;
        int minDistVertexNum = this.terminals[0];
        for (Integer vertexNumber : this.terminals) {
            if (vertexNumber != srcVertex && !addedTerminals.contains(vertexNumber) && distances[vertexNumber - 1] < minDistVertex) {
//                System.out.println("distance: " + distances[vertexNumber - 1]);
//                System.out.println("vertNum: " + vertexNumber);
                minDistVertex = distances[vertexNumber - 1];
                minDistVertexNum = vertexNumber;
            }
        }

        return minDistVertexNum;
    }

    public static int calcCost(ArrayList<Edge> edges) {
        int cost = 0;
        for (Edge edge : edges) {
            cost += edge.getWeight();
        }

        return cost;
    }


}
