package com.steinertree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Dijkstra {

    private static int minDistance(int[] dist, boolean[] included, int vertexCount) {
        // Initialize min value
        int min = Integer.MAX_VALUE, minIndex = -1;
        int vertexNumber;
        for (vertexNumber = 0; vertexNumber < vertexCount; vertexNumber++)
            if (!included[vertexNumber] && dist[vertexNumber] <= min) {
                min = dist[vertexNumber];
                minIndex = vertexNumber;
            }

        return minIndex;
    }

    public static Map<String, Object> apply(Map<Integer, Map<Integer, Integer>> graphNodeAdjacency, int src) {
        int vertexCount = graphNodeAdjacency.size();

        int index;
        int[] distances = new int[vertexCount];
        boolean[] includedVertices = new boolean[vertexCount];
        Map<Integer, Integer> parents = new HashMap<>();
        for (index = 1; index <= vertexCount; ++index) {
            if (index == src) {
                distances[index - 1] = 0;
            } else {
                distances[index - 1] = Integer.MAX_VALUE;
            }
            includedVertices[index - 1] = false;
        }
        Map<String, Object> path = new HashMap<>();
        ArrayList<Integer> intervals = new ArrayList<>();
        for (index = 0; index < vertexCount - 1; ++index) {
            // Pick the minimum distance vertex from the set of vertices
            // not yet processed. u is always equal to src in first
            // iteration.
            int minDistVertex = minDistance(distances, includedVertices, vertexCount);
            intervals.add(minDistVertex);
            // Mark the picked vertex as processed
            includedVertices[minDistVertex] = true;

            // Update dist value of the adjacent vertices of the
            // picked vertex.
            Map<Integer, Integer> adjacentVertices = graphNodeAdjacency.get(minDistVertex + 1);
            adjacentVertices.forEach((key, value) -> {
                if (!includedVertices[key - 1] && distances[minDistVertex] != Integer.MAX_VALUE && distances[minDistVertex] + value < distances[key - 1]) {
                    parents.put(key, minDistVertex + 1);
                    distances[key - 1] = distances[minDistVertex] + value;
                }
            });
        }
        path.put("distances", distances);
        path.put("parents", parents);
//        System.out.println("SRC VERT: " + src);
//        System.out.println(parents);
//        printSolution(distances, vertexCount);
        return path;

    }

    public static ArrayList<Integer> getPath(int vertexNumber, Map<Integer, Integer> parents, int srcVertex) {
        ArrayList<Integer> path = new ArrayList<>();
        path.add(srcVertex);
        return getPathRec(vertexNumber, parents, path);
    }

    public static ArrayList<Integer> getPathRec(int vertexNumber, Map<Integer, Integer> parents, ArrayList<Integer> path) {
        if (!parents.containsKey(vertexNumber)) {
            return path;
        }
        getPathRec(parents.get(vertexNumber), parents, path);
        path.add(vertexNumber);
        return path;
    }

    private static void printSolution(int[] dist, int vertexCount)
    {
        System.out.println("Vertex \t\t Distance from Source");
        for (int i = 0; i < vertexCount; i++)
            System.out.println(i + " \t\t " + dist[i]);
    }
}
