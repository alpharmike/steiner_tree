package com.steinertree;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) throws IOException {
        // use the above code as test
//        int vertexCount = 6;
//        Edge[] edges = new Edge[9];
//
//        for (int index = 0; index < 9; ++index) {
//            edges[index] = new Edge();
//        }
//
//        edges[0].getVertices().add(1);
//        edges[0].getVertices().add(2);
//        edges[0].setWeight(7);
//
//        edges[1].getVertices().add(1);
//        edges[1].getVertices().add(6);
//        edges[1].setWeight(6);
//
//        edges[2].getVertices().add(2);
//        edges[2].getVertices().add(6);
//        edges[2].setWeight(5);
//
//        edges[3].getVertices().add(2);
//        edges[3].getVertices().add(3);
//        edges[3].setWeight(1);
//
//        edges[4].getVertices().add(6);
//        edges[4].getVertices().add(5);
//        edges[4].setWeight(10);
//
//        edges[5].getVertices().add(3);
//        edges[5].getVertices().add(5);
//        edges[5].setWeight(3);
//
//        edges[6].getVertices().add(3);
//        edges[6].getVertices().add(4);
//        edges[6].setWeight(1);
//
//        edges[7].getVertices().add(4);
//        edges[7].getVertices().add(5);
//        edges[7].setWeight(1);
//
//        edges[8].getVertices().add(4);
//        edges[8].getVertices().add(6);
//        edges[8].setWeight(4);

//        edges[0].getVertices().add(0);
//        edges[0].getVertices().add(1);
//        edges[0].setWeight(7);
//
//        edges[1].getVertices().add(0);
//        edges[1].getVertices().add(5);
//        edges[1].setWeight(6);
//
//        edges[2].getVertices().add(1);
//        edges[2].getVertices().add(5);
//        edges[2].setWeight(5);
//
//        edges[3].getVertices().add(1);
//        edges[3].getVertices().add(2);
//        edges[3].setWeight(1);
//
//        edges[4].getVertices().add(5);
//        edges[4].getVertices().add(4);
//        edges[4].setWeight(10);
//
//        edges[5].getVertices().add(2);
//        edges[5].getVertices().add(4);
//        edges[5].setWeight(3);
//
//        edges[6].getVertices().add(2);
//        edges[6].getVertices().add(3);
//        edges[6].setWeight(1);
//
//        edges[7].getVertices().add(3);
//        edges[7].getVertices().add(4);
//        edges[7].setWeight(1);
//
//        edges[8].getVertices().add(3);
//        edges[8].getVertices().add(5);
//        edges[8].setWeight(4);

//        Graph graph = new Graph(vertexCount, edges);
//
//        Edge[] finalEdges = MinimumSpanningTree.apply(graph);
//        Graph newGraph = new Graph(graph.getVertexCount(), finalEdges);
//        Map<String, Object> res = Dijkstra.apply(newGraph.getAdjacentNodes(), 4);
//        ArrayList<Integer> a = Dijkstra.getPath(2, (Map<Integer, Integer>) res.get("parents"), 4);
//        System.out.println(a);
//        int[] terminals = new int[4];
//        terminals[0] = 1;
//        terminals[1] = 3;
//        terminals[2] = 5;
//        terminals[3] = 6;
//        SteinerTree steinerTree = new SteinerTree(newGraph, terminals);
//        ArrayList<Edge> a = steinerTree.apply();
//
//        System.out.println("final");
//        for (int edgeIndex = 0; edgeIndex < a.size(); ++edgeIndex) {
//            a.get(edgeIndex).showEdge();
//        }


//
//        Graph initGraph = new Graph(nodeCount, initEdges);
//        Edge[] newFinalEdges = MinimumSpanningTree.apply(initGraph);
//        System.out.println("MST");
//        for (int i = 0 ; i < newFinalEdges.length; ++i) {
//            newFinalEdges[i].showEdge();
//        }
//        Graph newFinalGraph = new Graph(initGraph.getVertexCount(), newFinalEdges);
//        Edge[] finalEdges = MinimumSpanningTree.apply(newFinalGraph);
//        int cost = 0;
//        for (int i = 0; i <  newFinalEdges.length; ++i) {
//            newFinalEdges[i].showEdge();
//            cost += newFinalEdges[i].getWeight();
//        }
//        System.out.println("cost: " + cost);
//        boolean cyclic = newFinalGraph.isCyclic();
//        System.out.println(cyclic);

//        Map<String, Object> res = Dijkstra.apply(newFinalGraph.getAdjacentNodes(), 19);
//        ArrayList<Integer> a = Dijkstra.getPath(543, (Map<Integer, Integer>) res.get("parents"), 19);
//        System.out.println(a);
//        System.out.println(res.toString());
//        for (int i = 0; i < 32; ++i) {
//            System.out.print(((int[])res.get("distances"))[i] + " ");
//        }
//        SteinerTree newSteinerTree = new SteinerTree(newFinalGraph, initTerminals);
//        ArrayList<Edge> resultEdges = newSteinerTree.apply();
//        System.out.println("ANSWER");
//        int cost = 0;
//
//        for (Edge edge : resultEdges) {
////            edge.showEdge();
//            cost += edge.getWeight();
//        }

//        Graph g = new Graph(550, resultEdges.toArray(new Edge[0]));
//        boolean cyclic = g.isCyclic();
//        System.out.println(cyclic);
//        System.out.println("cost: " + cost);
//        File obj = new File("outputs/a.out");
//
//        if (obj.createNewFile()) {
//
//        }
//        try (Stream<Path> walk = Files.walk(Paths.get("files"))) {
//
//            List<String> result = walk.filter(Files::isRegularFile)
//                    .map(Path::toString).collect(Collectors.toList());
//
//            result.forEach();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        File folder = new File("files");
        File[] files = folder.listFiles();


        for (int i = 0; files != null && i < files.length; ++i) {
            Map<String, Object> fileInfo =  FileReader.readFile(files[i].getPath());
            int nodeCount = (int) fileInfo.get("nodeCount");
            Edge[] edges = (Edge[]) fileInfo.get("edges");
            int[] terminals = (int[]) fileInfo.get("terminals");
            Graph graph = new Graph(nodeCount, edges);
            Edge[] MSTEdges = MinimumSpanningTree.apply(graph);
            Graph finalGraph = new Graph(graph.getVertexCount(), MSTEdges);
            SteinerTree steinerTree = new SteinerTree(finalGraph, terminals);
            ArrayList<Edge> resultEdges = steinerTree.apply();
            int cost = SteinerTree.calcCost(resultEdges);

            StringTokenizer stringTokenizer = new StringTokenizer(files[i].getName(), ".");
            String fileName = stringTokenizer.nextToken();
            String outFilePath = "outputs/" + fileName + ".out";
            File newFile = new File(outFilePath);
            if (newFile.createNewFile()) {
                FileWriter fileWriter = new FileWriter(outFilePath, true);
                fileWriter.write(String.format("Cost: %d\n", cost));
                fileWriter.write(String.format("Edges: %d\n", resultEdges.size()));

                for (Edge edge : resultEdges) {
                    fileWriter.write(String.format("E %d %d\n", edge.getVertices().get(0), edge.getVertices().get(1)));
                }
                fileWriter.close();
            }
        }

//            Map<String, Object> fileInfo =  FileReader.readFile("files/bip42p.stp");
//            int nodeCount = (int) fileInfo.get("nodeCount");
//            Edge[] edges = (Edge[]) fileInfo.get("edges");
//            int[] terminals = (int[]) fileInfo.get("terminals");
//            Graph graph = new Graph(nodeCount, edges);
//            Edge[] MSTEdges = MinimumSpanningTree.apply(graph);
//            Graph finalGraph = new Graph(graph.getVertexCount(), MSTEdges);
//            SteinerTree steinerTree = new SteinerTree(finalGraph, terminals);
//            ArrayList<Edge> resultEdges = steinerTree.apply();
//            int cost = SteinerTree.calcCost(resultEdges);
//            System.out.println(cost);

    }
}
