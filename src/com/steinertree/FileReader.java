package com.steinertree;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class FileReader {
    public static Map<String, Object> readFile(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        Scanner reader = new Scanner(file);
        String line;
        while (!(line = reader.nextLine()).equals("SECTION Graph")) ;
        line = reader.nextLine(); // get nodes line
        StringTokenizer nodeTokenizer = new StringTokenizer(line, " ");
        nodeTokenizer.nextToken(); // neglect the beginning word
        int nodeCount = Integer.parseInt(nodeTokenizer.nextToken());
        System.out.println(nodeCount);

        line = reader.nextLine();
        StringTokenizer edgeTokenizer = new StringTokenizer(line, " ");
        edgeTokenizer.nextToken();
        int edgeCount = Integer.parseInt(edgeTokenizer.nextToken());
        System.out.println(edgeCount);
        int index = 0;
        Edge[] edges = new Edge[edgeCount];

        // extracting edges
        while (!(line = reader.nextLine()).equals("End")) {
            StringTokenizer tokenizer = new StringTokenizer(line, " ");
            tokenizer.nextToken();
            int firstVertex = Integer.parseInt(tokenizer.nextToken());
            int secondVertex = Integer.parseInt(tokenizer.nextToken());
            int weight = Integer.parseInt(tokenizer.nextToken());
            Edge edge = new Edge(firstVertex, secondVertex, weight);
            edges[index] = edge;
            ++index;
        }

        while (!(line = reader.nextLine()).equalsIgnoreCase("Section Terminals")) ;
        line = reader.nextLine();
        StringTokenizer terminalTokenizer = new StringTokenizer(line, " ");
        terminalTokenizer.nextToken();
        int terminalCount = Integer.parseInt(terminalTokenizer.nextToken());
        System.out.println(terminalCount);
        int[] terminals = new int[terminalCount];
        index = 0;

        // extracting terminals
        while (!(line = reader.nextLine()).equals("End")) {
            StringTokenizer tokenizer = new StringTokenizer(line, " ");
            tokenizer.nextToken();
            terminals[index] = Integer.parseInt(tokenizer.nextToken());
            ++index;
        }

        System.out.println(Arrays.toString(terminals));
        Map<String, Object> fileInfo = new HashMap<>();
        fileInfo.put("nodeCount", nodeCount);
        fileInfo.put("edges", edges);
        fileInfo.put("terminals", terminals);

        return fileInfo;
    }
}
