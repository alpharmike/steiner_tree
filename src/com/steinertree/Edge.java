package com.steinertree;

import java.util.ArrayList;

public class Edge implements Comparable<Edge> {
    private int weight;
    private ArrayList<Integer> vertices; // the graph is undirected, so we don't consider source nad destination

    public Edge(int weight, ArrayList<Integer> vertices) {
        this.weight = weight;
        this.vertices = vertices;
    }

    public Edge(int weight) {
        this.weight = weight;
        this.vertices = new ArrayList<>();
    }

    public Edge(int firstVertex, int secondVertex, int weight) {
        this.vertices = new ArrayList<>();
        this.vertices.add(firstVertex);
        this.vertices.add(secondVertex);
        this.weight = weight;
    }

    public Edge() {
        this.vertices = new ArrayList<>();
    }

    @Override
    public int compareTo(Edge otherEdge) {
        return this.getWeight() - otherEdge.getWeight();
    }



    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public ArrayList<Integer> getVertices() {
        return vertices;
    }

    public void setVertices(ArrayList<Integer> vertices) {
        this.vertices = vertices;
    }

    public void showEdge() {
        System.out.println(this.getVertices().get(0) + " --- " + this.getVertices().get(1) + " == " + this.getWeight());
    }
}
