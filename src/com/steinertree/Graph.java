package com.steinertree;

import java.util.*;

public class Graph {
    private int vertexCount;
    private Edge[] edges;
    private LinkedList<Integer>[] adj;

    public Graph(int vertexCount, Edge[] edges) {
        this.vertexCount = vertexCount;
        this.edges = edges;
        this.initAdj();
        this.fillAdj();
    }

    public int getVertexCount() {
        return vertexCount;
    }

    public void setVertexCount(int vertexCount) {
        this.vertexCount = vertexCount;
    }

    public Edge[] getEdges() {
        return edges;
    }

    public void setEdges(Edge[] edges) {
        this.edges = edges;
    }

    public Edge getEdge(int srcVertex, int destVertex) {
        int edgeIndex;
        for (edgeIndex = 0; edgeIndex < this.edges.length; ++edgeIndex) {
            int edgeSrc = this.edges[edgeIndex].getVertices().get(0);
            int edgeDest = this.edges[edgeIndex].getVertices().get(1);
            if ((srcVertex == edgeSrc && destVertex == edgeDest) || (srcVertex == edgeDest && destVertex == edgeSrc)) {
                return this.edges[edgeIndex];
            }
        }
        return null;
    }

    public Map<Integer, Map<Integer, Integer>> getAdjacentNodes() {
        int edgeIndex;
        Map<Integer, Map<Integer, Integer>> adjacentNodes = new HashMap<>();
        for (edgeIndex = 0; edgeIndex < this.getEdges().length; ++edgeIndex) {
            int edgeSrc = this.edges[edgeIndex].getVertices().get(0);
            int edgeDest = this.edges[edgeIndex].getVertices().get(1);
            int edgeWeight = this.edges[edgeIndex].getWeight();
            if (!adjacentNodes.containsKey(edgeSrc)) {
                Map<Integer, Integer> currNodeAdjacent = new HashMap<>();
                currNodeAdjacent.put(edgeDest, edgeWeight);
                adjacentNodes.put(edgeSrc, currNodeAdjacent);
            } else {
                adjacentNodes.get(edgeSrc).putIfAbsent(edgeDest, edgeWeight);
            }

            if (!adjacentNodes.containsKey(edgeDest)) {
                Map<Integer, Integer> currNodeAdjacent = new HashMap<>();
                currNodeAdjacent.put(edgeSrc, edgeWeight);
                adjacentNodes.put(edgeDest, currNodeAdjacent);
            } else {
                adjacentNodes.get(edgeDest).putIfAbsent(edgeSrc, edgeWeight);
            }

        }

        return adjacentNodes;
    }

    public int find(Subset[] subsets, int vertex) {
        if (subsets[vertex - 1].getParent() != vertex) {
            subsets[vertex - 1].setParent(this.find(subsets, subsets[vertex - 1].getParent()));
        }
        return subsets[vertex - 1].getParent();
    }

    public void union(Subset[] subsets, int firstVertex, int secondVertex) {
        int firstVertexRoot = this.find(subsets, firstVertex);
        int secondVertexRoot = this.find(subsets, secondVertex);

        if (subsets[firstVertexRoot - 1].getSize() < subsets[secondVertexRoot - 1].getSize()) {
            subsets[firstVertexRoot - 1].setParent(secondVertexRoot);
        } else if (subsets[secondVertexRoot - 1].getSize() < subsets[firstVertexRoot - 1].getSize()) {
            subsets[secondVertexRoot - 1].setParent(firstVertexRoot);
        } else {
            subsets[firstVertexRoot - 1].setParent(secondVertexRoot);
            subsets[secondVertexRoot - 1].setSize(subsets[secondVertexRoot - 1].getSize() + 1);
        }
    }

    public Subset[] initializeSubsets() {
        Subset[] subsets = new Subset[this.vertexCount];
        int vertexNumber;
        for (vertexNumber = 1; vertexNumber <= vertexCount; ++vertexNumber) {
            subsets[vertexNumber - 1] = new Subset(vertexNumber, 0);
        }
        return subsets;
    }


    private void initAdj() {
        this.adj = new LinkedList[this.vertexCount];
        for (int i = 0; i < vertexCount; ++i)
            adj[i] = new LinkedList();
    }

    void addEdge(int v,int w) {
        adj[v].add(w);
        adj[w].add(v);
    }

    Boolean isCyclicUtil(int v, Boolean visited[], int parent)
    {
        // Mark the current node as visited
        visited[v] = true;
        Integer i;

        // Recur for all the vertices adjacent to this vertex
        Iterator<Integer> it = adj[v].iterator();
        while (it.hasNext())
        {
            i = it.next();

            // If an adjacent is not visited, then recur for that
            // adjacent
            if (!visited[i])
            {
                if (isCyclicUtil(i, visited, v))
                    return true;
            }

            // If an adjacent is visited and not parent of current
            // vertex, then there is a cycle.
            else if (i != parent)
                return true;
        }
        return false;
    }

    Boolean isCyclic()
    {
        // Mark all the vertices as not visited and not part of
        // recursion stack
        Boolean visited[] = new Boolean[this.vertexCount];
        for (int i = 0; i < this.vertexCount; i++)
            visited[i] = false;

        // Call the recursive helper function to detect cycle in
        // different DFS trees
        for (int u = 0; u < this.vertexCount; u++)
            if (!visited[u]) // Don't recur for u if already visited
                if (isCyclicUtil(u, visited, -1))
                    return true;

        return false;
    }

    private void fillAdj() {
        for (int i = 0; i < this.edges.length; ++i) {
            this.addEdge(this.edges[i].getVertices().get(0) - 1, this.edges[i].getVertices().get(1) - 1);
        }
    }


//    public Edge[] applyKruskalMST() {
//        Arrays.sort(edges);
//
//        Subset[] subsets = this.initializeSubsets();
//        Edge[] finalEdges = new Edge[this.vertexCount - 1]; // the number of final edges will be the number of vertices - 1
//
//
//        int edgeIndex, insertedEdgeIndex;
//        insertedEdgeIndex = 0;
//        for (edgeIndex = 0; edgeIndex < this.edges.length - 1; ++edgeIndex) {
//            if (insertedEdgeIndex == this.vertexCount - 1) {
//                break;
//            }
//            Edge currentEdge = this.edges[edgeIndex];
//
//            int firstVertexRoot = this.find(subsets, currentEdge.getVertices().get(0));
//            int secondVertexRoot = this.find(subsets, currentEdge.getVertices().get(1));
//            if (firstVertexRoot != secondVertexRoot) { // check for cycle
//                finalEdges[insertedEdgeIndex] = currentEdge;
//                union(subsets, firstVertexRoot, secondVertexRoot);
//                ++insertedEdgeIndex;
//            }
//        }
//
//        return finalEdges;
//    }
}
