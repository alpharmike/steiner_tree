package com.steinertree;

import java.util.Arrays;

public class MinimumSpanningTree {

    public static Edge[] apply(Graph graph) {
        Arrays.sort(graph.getEdges());

        Subset[] subsets = graph.initializeSubsets();
        Edge[] finalEdges = new Edge[graph.getVertexCount() - 1]; // the number of final edges will be the number of vertices - 1


        int edgeIndex, insertedEdgeIndex;
        insertedEdgeIndex = 0;
        for (edgeIndex = 0; edgeIndex < graph.getEdges().length - 1; ++edgeIndex) {
            if (insertedEdgeIndex == graph.getVertexCount() - 1) {
                break;
            }
            Edge currentEdge = graph.getEdges()[edgeIndex];

            int firstVertexRoot = graph.find(subsets, currentEdge.getVertices().get(0));
            int secondVertexRoot = graph.find(subsets, currentEdge.getVertices().get(1));
            if (firstVertexRoot != secondVertexRoot) { // check for cycle by comparing the subsets
                finalEdges[insertedEdgeIndex] = currentEdge;
                graph.union(subsets, firstVertexRoot, secondVertexRoot);
                ++insertedEdgeIndex;
            }
        }

        return finalEdges;
    }
}
