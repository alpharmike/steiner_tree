package com.steinertree;

public class Subset {
    private int parent;
    private int size;

    public Subset(int parent, int size) {
        this.parent = parent;
        this.size = size;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
